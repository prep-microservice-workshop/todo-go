package service

import (
	"context"
	"myapp/config"
	"myapp/graph/model"

	"github.com/vektah/gqlparser/gqlerror"
	"gitlab.com/prep-microservice-workshop/utils-go.git/middleware"
	"gorm.io/gorm"
)

func TodoCreate(ctx context.Context, newTodoText string) (*model.Todo, error) {

	loggedInUser := middleware.AuthContext(ctx)
	if loggedInUser == nil {
		return nil, gqlerror.Errorf("You are not logged In!")
	}

	db := config.GetDB()

	todo := model.Todo{
		Text:   newTodoText,
		IsDone: false,
		UserID: loggedInUser.ID,
	}

	err := db.Model(&model.Todo{}).Create(&todo).Error
	if err != nil {
		panic(err)
	}

	return &todo, nil
}

func TodoStatusUpdate(ctx context.Context, id int, status bool) (*model.Todo, error) {

	loggedInUser := middleware.AuthContext(ctx)
	if loggedInUser == nil {
		return nil, gqlerror.Errorf("You are not logged In!")
	}

	db := config.GetDB()

	err := db.Model(&model.Todo{}).
		Where("id = ? AND user_id = ?", id, loggedInUser.ID).Update("is_done", status).Error

	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, gqlerror.Errorf("User not found, Invalid token!")
		} else {
			panic(err)
		}
	}

	return TodoGetByID(id)
}

func TodoGetByID(id int) (*model.Todo, error) {

	db := config.GetDB()

	var todo model.Todo

	err := db.Model(&model.Todo{}).Where("id = ?", id).Take(&todo).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, gqlerror.Errorf("Todo not found!")
		} else {
			panic(err)
		}
	}

	return &todo, nil
}

func TodoDelete(ctx context.Context, id int) *int {
	loggedInUser := middleware.AuthContext(ctx)
	if loggedInUser == nil {
		return nil
	}

	db := config.GetDB()

	err := db.Model(&model.Todo{}).
		Where("id = ? AND user_id = ?", id, loggedInUser.ID).Delete(&model.Todo{}).Error

	if err != nil {
		panic(err)
	}

	return &id
}

func TodoGetByWhereInUserIDs(userIDs []int) []*model.Todo {

	db := config.GetDB()

	var todos []*model.Todo

	err := db.Model(&model.Todo{}).Where("user_id IN (?)", userIDs).Find(&todos).Error
	if err != nil {
		panic(err)
	}

	return todos
}

func TodosLoaders(keys []int) ([][]*model.Todo, []error) {

	todos := TodoGetByWhereInUserIDs(keys)
	todosMappedByUserIDs := map[int][]*model.Todo{}

	for _, todo := range todos {
		todosMappedByUserIDs[todo.UserID] = append(todosMappedByUserIDs[todo.UserID], todo)
	}

	result := make([][]*model.Todo, len(keys))
	for i, id := range keys {
		result[i] = todosMappedByUserIDs[id]
	}

	return result, nil
}
