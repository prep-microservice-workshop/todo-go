package dataloader

import (
	"context"
	"time"

	"myapp/service"

	"github.com/gin-gonic/gin"
)

type ctxKeyType string

const (
	ctxKey ctxKeyType = "dataloaders"
)

type loaders struct {
	Todos *TodosLoaders
}

func DataloaderMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		ldrs := loaders{}
		wait := 10 * time.Millisecond
		max := 150

		ldrs.Todos = NewTodosLoaders(TodosLoadersConfig{
			Fetch:    service.TodosLoaders,
			Wait:     wait,
			MaxBatch: max,
		})

		dataloaderCtx := context.WithValue(c.Request.Context(), ctxKey, ldrs)
		c.Request = c.Request.WithContext(dataloaderCtx)
		c.Next()
	}
}

func CtxLoaders(ctx context.Context) loaders {
	return ctx.Value(ctxKey).(loaders)
}
