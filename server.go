package main

import (
	"context"
	"errors"
	"log"
	"myapp/graph"
	"myapp/graph/generated"
	"net/http"
	"os"

	"myapp/config"
	"myapp/dataloader"

	"gitlab.com/prep-microservice-workshop/utils-go.git/middleware"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/gin-gonic/gin"
	"github.com/rs/cors"
)

const defaultPort = "8081"

func init() {
	config.ConnectGorm()
}

func main() {

	db := config.GetDB()
	sqlDB, _ := db.DB()
	defer sqlDB.Close()

	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	c := generated.Config{Resolvers: &graph.Resolver{}}
	srv := handler.NewDefaultServer(generated.NewExecutableSchema(c))
	srv.SetRecoverFunc(func(ctx context.Context, err interface{}) error {
		log.Printf("[panic] %v", err)
		return errors.New("internal system error")
	})

	router := gin.New()
	router.Use(middleware.Authorize())
	router.Use(dataloader.DataloaderMiddleware())

	handler := cors.New(cors.Options{
		AllowedHeaders: []string{"*"},
		AllowedMethods: []string{"GET", "POST", "PUT", "HEAD", "OPTIONS"},
	}).Handler(srv)

	router.GET("/", func(c *gin.Context) {
		playground.Handler("GraphQL playground", "/query").ServeHTTP(c.Writer, c.Request)
	})
	router.POST("/query", func(c *gin.Context) {
		handler.ServeHTTP(c.Writer, c.Request)
	})

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Fatal(http.ListenAndServe(":"+port, router))
}
